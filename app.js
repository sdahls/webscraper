const express = require('express')
const path = require('path')
const data = require('./data.json')
const tableGetter = require('./tableGetter')
const app = express();
// importing dependencies



const { PORT = 8080 } = process.env;
   
    app.use(express.json()); //mothod for recognizing json with express
    app.use(express.urlencoded( { extended: true } )); //

  
//endpoint for sending the data from the server
app.get('/data', (req,res) => {
    let url=req.body.url//getting the url from the req-body
    tableGetter(url)//calling the method for scraping a table on wikipedia
   
   res.send(data) //sending the data to a front
   res.end()

 })



app.listen(PORT, () => console.log(`Server started on port:  ${PORT}`))