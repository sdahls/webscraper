# Web Scraper App using Cheerio

### Oskar Olsson & Sven "Svennis" Dahlström

Application that has one single endpoint (/data) that takes <br>
one parameter from the request header (url) and then scrapes <br>
, preferably, a Wikipedia-page for tables with the class name .wikitable. <br>
<br>

Feel free to scrape different wikipedia sites and look at the astonishing result