const cheerio = require('cheerio');
const axios=require('axios')
const request=require('request')
const fs=require('fs');
const { parse } = require('path');


let buildingArray = [];

module.exports =function getTable(url){ //exporting the function to be used by the app.js

    request(url, (error, response, html)=>{

        const $ = cheerio.load(html) //using cherio to gett the html
        
       $('.wikitable').first().each((index,tablerow)=>{ //getting the first table with the class of wikitable
          const tablestring=$(tablerow).find('tbody') //taking the table, and finding the tablebody and putting it and its child in a string
        
          

        for(i=1; i<tablestring.find('tr').length;i++){//itterationg through each talbe row
            

             let buildingObject={} //instanciating a new buildingobjet, wich will look att the talbe header and creat the keys
               for (let index = 0; index <tablestring.find('th').length; index++) { //itterating through the tablerow
               let key=tablestring.find('th').eq(index).text()  //finding the keys for each table collumn
               key= key.replace(/(\r\n|\n|\r)/gm, "").trim().toLowerCase(); //trimming the key
               // console.log("index:" + index + "  " +tablestring.find('tr').eq(i).children().eq(index).text())
              let temp=tablestring.find('tr').eq(i).children().eq(index).text() //finding table cell with the help of the itterations
             //temp.trim()
                buildingObject[key] =  temp.trim() //creating a key value pair from the table headeers and the tablerow cells
              //dataArray.push(temp)
               }
                
           buildingArray.push(buildingObject); //putting each complete object into an array
       }
    
      })
      let buildingJson=JSON.stringify(buildingArray, null, 2); //creating a json object from the array
      
      fs.writeFile(__dirname+'/data.json',buildingJson, (err, content)=> {}) //wrighting the json object to a file
    })
                  
}



